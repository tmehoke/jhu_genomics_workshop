script_location="$(perl -MCwd=abs_path -le 'print abs_path(shift)' $(which $(basename $0)))"
# Software_location="$HOME/Documents/Software/"
KRAKEN_DIR="$HOME/bin/kraken_dir"
BEAST_dir="$HOME/bin/BEAST_dir"
LAUNCHER_dir="$HOME/.local/share/applications"
kraken_database_location="$HOME/Documents/database/flukraken-2019-06-24"
Database_location="$HOME/Documents/database/"
###Install minknow software from https://github.com/rrwick/MinION-desktop/  github repo
if [ ! -e "/opt/ui/MinKNOW" ]; then	
	#install MinKNOW
	wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
	echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
	sudo apt-get update
	sudo apt-get -y install minknow-nc
	sudo apt-get -y install minknow-nc

fi
#install tempest
###Website to download tempest is currently down 
# if [ ! -d ${Software_location}"TempEst*" ]; then
# 	wget 'http://tree.bio.ed.ac.uk/download.php?id=102&num=3' -O  ${Software_location}"TempEst_v1.5.3.tgz"
# 	tar -xvzf $Software_location'TempEst_v1.5.3.tgz' --directory $Software_location 
# 	sudo chmod 755 $Software_location'TempEst_v1.5.3/bin/tempest'
# 	ln -sf $Software_location'TempEst_v1.5.3/bin/tempest' ~/bin/
# 	ln -sf $Software_location'TempEst_v1.5.3/lib/tempest.jar' ~/bin/
# fi
if [[ ! -x "$(command -v tempest)" ]] || ([ -L "tempest" ]) && ([ ! -a "tempest" ]); then
	#Install tempest locally
	tar -xvzf "$script_location/placeholder_software/TempEst_v1.5.3.tgz" --directory "$script_location/Software/"
	sudo chmod 755 "$script_location/Software/TempEst_v1.5.3/bin/tempest"
	ln -sf "$script_location/Software/TempEst_v1.5.3/bin/tempest" ~/bin/
	ln -sf "$script_location/Software/TempEst_v1.5.3/lib/tempest.jar" ~/bin/
fi
if [[ ! -x "$(command -v subl)" ]] || ([ -L "subl" ]) && ([ ! -a "subl" ]); then
	#######sublime text install
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
	echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
	sudo apt-get update
	sudo apt-get install sublime-text
fi
if [[ ! -x "$(command -v minimap2)" ]] || ([ -L "minimap2" ]) && ([ ! -a "minimap2" ]); then
	cd "$script_location/Software/"
	git clone https://github.com/lh3/minimap2.git
	cd minimap2
	make
	ln -sf "$script_location/Software/minimap2/minimap2" ~/bin/bin/
	cd "$script_location"
fi
if ! [  -d ${Database_location}minikraken_2017* ]; then
	wget https://ccb.jhu.edu/software/kraken/dl/minikraken_20171019_4GB.tgz -P $Database_location
	tar -xvzf ${Database_location}minikraken_20171019_4GB.tgz  --directory $Database_location
	rm ${Database_location}minikraken_20171019_4GB.tgz 
fi

#Enable offline mode for MinKNOW Software
#Deprecated?
bash linux_offline_minknow.sh

