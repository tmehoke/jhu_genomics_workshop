/* WARNING:

The base version of this script is found in:

	jhu_genomics_workshop/Install_Scripts/renderer.js

That script is modified for environment variables and copied to:

	jhu_genomics_workshop/app/renderer.js

All edits should be made in the first script (in Install_Scripts).

 */

// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.


const homedir = require("os").homedir();
const remote = require('electron').remote

const mkdirp = require('mkdirp');
var dat_loc = "<KRAKEN_DIR>"
var orig = dat_loc
const options = {
    type: 'question',
    buttons: ['Cancel'],
    defaultId: 2,
    title: 'Alert there is an error',
    message: '',
  };
///////////////Check if table files are present and override indexhtml elements if so
var old_json = ""
// read_table_files()
// if (fs.existsSync("<APP_DIR>/data/table.txt")) {
// 	fs.readFile("<APP_DIR>/data/table.txt", (err,data)=>{
// 		old_json = JSON.parse(data)
// 		write_table(old_json)
// 	})
// }
// else{
// 	console.log("file doesnt exist for tables, making one")
// 	jso = read_table_files()
// 	old_json = jso
// 	write_table(old_json)
// 	write_file(jso, 0)
// }


// Functions!!////////////////////////////////////////
function show_process(){
  const BrowserWindow = remote.BrowserWindow;
  const win2 = new BrowserWindow({ width: 500, height: 400, x: 800,
        y: 0 })
  win2.loadFile('<APP_DIR>/tabs/fastq/fastq3.html')
  win2.webContents.openDevTools()
	win2.webContents.on("did-finish-load", () =>{
		// console.log(win2.webContents())
		// console.log(remote.getCurrentWindow.getURL())
	})
}
var x = document.getElementById("database_loc")
document.getElementById("default_loc").innerText += dat_loc
x.addEventListener("change", function(){
	// (console.log(x.files[0].path))
	dat_loc  = x.files[0].path
})

//Input file for process oxford (contains MinION Reads)
$('#myInput').on("click", function(){
	const { dialog } = require('electron').remote
	 dialog.showOpenDialog({
        properties: ['openDirectory']
    }, function (files) {
        if (files !== undefined) {
            document.getElementById("myInput").files = [files]
            console.log( document.getElementById("myInput").files)
        }
    });
})
//Process oxford run on MinION Reads
function run_process_oxford(){
	const { dialog } = require('electron').remote
	var folder = document.getElementById("myInput")
	if(!(folder.files)){
		options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		const dir = "."
		const command = "lxterminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/minion_commands.sh "
		var command_fill = command.concat(" process_oxford.sh -p "+folder.files[0]+" -k "+dat_loc+"; exec bash'\"")
		exec(command_fill)
	}
}
//Find the database location and process it with process_krakendb.sh
$('#database_loc_process').on("click", function(){
	if (orig != dat_loc){
		const command = "lxterminal --command=\"/bin/bash/bin/bash/bin/bash -c '<APP_DIR>/pre-commands/process_kraken.sh "+dat_loc+"; exec bash'\""
		exec(command)
	}
})
$('#database_loc').on("click", function(){
	const { dialog } = require('electron').remote
	 dialog.showOpenDialog({
        properties: ['openDirectory']
    }, function (files) {
        if (files !== undefined) {
            document.getElementById("database_loc").files = [files]
            dat_loc = files[0]
        }
    });
})


function reference_alignment() {
}
$('#minimap_align').on("click", function(){
	if(!(document.getElementById("split_fastq_file").files[0])){
		const { dialog } = require('electron').remote
		options.message ="No FASTQ File used to align to reference FASTA file"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if(!( document.getElementById("reference_file").files[0])){
		const { dialog } = require('electron').remote
		options.message ="No Reference Fasta File selected"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		var folder = document.getElementById("myInput")
		if(!(folder.files[0])){
			const { dialog } = require('electron').remote
			options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
			dialog.showMessageBox(null, options, (response) => {
			});
		} else{
			mkdirp(folder.files[0] + "/output/4-reference_alignment", function(err) { console.log(err); })
			var ref_file = document.getElementById("reference_file").files[0].path
			var split_fastq_file = document.getElementById("split_fastq_file").files[0].path
			var output_path = folder.files[0] + "/output/4-reference_alignment/"
			var fastq_base = split_fastq_file.substr(split_fastq_file.lastIndexOf("\/")+1);
			var output_base = fastq_base.replace(/\.fastq$/, '')
			var output_file = output_path + "/" + output_base
			var command = "lxterminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/minimap2_align.sh "+ref_file+" "+split_fastq_file+" "+output_file+"; exec bash '\""
			exec(command)
		}
	}
})
$('#make_consensus').on("click", function(){
	if(!(document.getElementById("alignment_file").files[0])){
		const { dialog } = require('electron').remote
		options.message ="No alignment file selected (.sam file extension)"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if(!(document.getElementById("reference_file").files[0])){
		const { dialog } = require('electron').remote
		options.message ="No Reference Fasta File selected"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		var folder = document.getElementById("myInput")
		if(!(folder.files)){
			const { dialog } = require('electron').remote
			options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
			dialog.showMessageBox(null, options, (response) => {
			});
		} else{
			mkdirp(folder.files[0] + "/output/5-consensus-sequences", function(err) { console.log(err); })
			var ref_file = document.getElementById("reference_file").files[0].path
			var alignment_file = document.getElementById("alignment_file").files[0].path
			var bam_file = alignment_file.replace(/\.sam/, '.bam')
			var alignment_base = alignment_file.substr(alignment_file.lastIndexOf("\/")+1);
			var output_path = folder.files[0] + "/output/5-consensus-sequences"
			var command = "lxterminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/bam_tofasta.sh "+alignment_file+" "+ref_file+" "+bam_file+" "+output_path+"; exec bash'\""
			execSync(command)
		}
	}
})

$('#BLAST_read_top').on("click", function(){
	var folder = document.getElementById("myInput")
	if(!(folder.files)){
		const { dialog } = require('electron').remote
		options.message ="No directory specified for blast formatting. Please choose a base directory from process_oxford"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
	  var command = "rm -rf "+folder.files[0]+"/output/blast.fasta"+" && awk -F ' '  '{if (FNR==1) { x=FILENAME; gsub(/.*\\//, \"\", x); gsub(/.+/, \">\", $1);print $1x} else if (FNR==2){print}}' "+folder.files[0] +"/output/*.fastq >> "+folder.files[0]+"/output/blast.fasta"
	  exec(command)
	  command = "lxterminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/remote_BLAST.sh " + folder.files[0]+"/output/blast.fasta "+folder.files[0]+"/output/blast.out; exec'\"" 
	  exec(command)
	}
})
$('.sbutton_queryNCBI').on("click", function(){
	var format_dir = "/"
	if (this.value == "general"){
		var folder = document.getElementById("myInput")
		format_dir += "output/"
	}
	else{
		var folder = document.getElementById("split_segments_fastq")
	}
	if(!(folder.files)){
		const { dialog } = require('electron').remote
		options.message ="No directory specified for blast formatting. Please choose a base directory (above) from process_oxford output "
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
	  var command = "rm -rf "+folder.files[0]+"/output/blast.fasta"+" && awk -F ' '  '{if (FNR==1) { x=FILENAME; gsub(/.*\\//, \"\", x); gsub(/.+/, \">\", $1);print $1x} else if (FNR==2){print}}' "+folder.files[0] +format_dir+"*.fastq >> "+folder.files[0]+format_dir+"blast.fasta"
	  exec(command)
	  command = "firefox https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastSearch"
	  exec(command)
	}
})
document.getElementById("Run_split_segments").addEventListener("click", function(){
	var report_file = document.getElementById("report_file")
	var fastq_file = document.getElementById("fastq_file")
	var kraken_file = document.getElementById("kraken_file")
	const { dialog } = require('electron').remote
	if (!(kraken_file)){
		options.message ="No kraken file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken) "
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if (!(fastq_file)){
		options.message ="No fastq file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.fastq) "
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if (!(report_file)){
		options.message ="No full string kraken report file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken.report.full) "
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		var folder = document.getElementById("myInput")
		if(!(folder.files)){
			const { dialog } = require('electron').remote
			options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
			dialog.showMessageBox(null, options, (response) => {
			});
		} else{
			mkdirp(folder.files[0] + "/output/2-split-level", function(err) { console.log(err); })
			var command = "lxterminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/split_segments_pre.sh "+fastq_file.files[0].path
			command += " "+report_file.files[0].path
			command += " "+kraken_file.files[0].path
			var parent = fastq_file.files[0].path.substr(0, fastq_file.files[0].path.lastIndexOf("\/"));
			var base = fastq_file.files[0].path.substr(fastq_file.files[0].path.lastIndexOf("\/")+1);
			base = base.substr(0, base.lastIndexOf("\."));
			command += " "+parent+"/2-split-level/"+base+"/";
			command += "; exec bash'\""
			exec(command)
		}
	}
})

$( "[contenteditable=true]" ).each(function(){
	$(this).append("<button class='add'>+</button>")
	$(this).find('.add').click(function(d){
		var row = $(this).parent().find("tr").first().clone()
		$(row).find("td").each(function(){
			this.innerHTML=""
		})
		$("<tr>"+row.html()+"</tr>").insertAfter($(this).parent().find("tr").last())
	})
	$(this).append("<button class='reduce'>-</button>")
	$(this).find(".reduce").click(function(){
		$(this).parent().find("tr").last().remove()
	})
	// $(this).append("<button class='reset'>Reset</button>")
	// $(this).find(".reset").click(function(){

	// })
})
$('#Blast_split_segments').on("click", function(){

})
$("#Reference_generation").on("click", function(){
	const { dialog } = require('electron').remote
//	if (!(document.getElementById("kraken_ref_file"))){
//		options.message ="No reference FASTA File chosen "
//		dialog.showMessageBox(null, options, (response) => {
//		});
//	}
//	else
	if (!(document.getElementById("kraken_file2").files[0])){
		options.message ="No kraken report chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken.report.full) "
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		var folder = document.getElementById("myInput")
		if(!(folder.files)){
			const { dialog } = require('electron').remote
			options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
			dialog.showMessageBox(null, options, (response) => {
			});
		} else{
			mkdirp(folder.files[0] + "/output/3-closest-reference", function(err) { console.log(err); })
			var kraken_file = document.getElementById("kraken_file2").files[0].path;
			var kraken_DB = dat_loc;
			var parent = kraken_file.substr(0, kraken_file.lastIndexOf("\/"));
			var base = kraken_file.substr(kraken_file.lastIndexOf("\/")+1);
			base = base.substr(0, base.indexOf("\."));
			var reference_fasta = folder.files[0] + "/output/3-closest-reference/"+base+"-closest-reference.fasta";
			var command = "lxterminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/closest_reference.sh "+kraken_file +" "+dat_loc+" "+reference_fasta+"; exec bash'\""
			console.log(command)
			execSync(command)
		}
	}

})

///////Set interval to see if changes in table structure on input/////
// setInterval(function(outnew){
// 	out  = read_table_files()
// 	write_file(out, 1)
// }, 6000);


// function write_table(json){
// 	for (var table in old_json.tables){
// 		var x = document.querySelector(old_json.tables[table].name)
// 		for (var row=0; row < x.rows.length; row +=1){
// 			for (var cell = 0; cell < x.rows[row].cells.length; cell +=1){
// 				x.rows[row].cells[cell].innerText = old_json.tables[table].children[row].cells[cell].id
// 			}
// 		}
// 	}
// }

// function write_file(data, tr){
// 	fs.writeFile('<APP_DIR>/data/table.txt', JSON.stringify(data, null, 2), (err) => {
// 	 if (err) throw err;
// 	  console.log('The file has been saved!');
// 	});
// 	if (JSON.stringify(old_json) != JSON.stringify(data) || tr ==0){
// 		old_json = data
// 		fs.writeFile('<APP_DIR>/data/table.txt', JSON.stringify(data, null, 2), (err) => {
// 				console.log("Different table structures. Saving now!")
// 				  if (err) throw err;
// 		});
// 	}
// }

// function read_table_files(){
// 	var outnew = '{ "tables": [\n'
// 	var tables = ['table1', 'table2']
// 	for (var table in tables){
// 		outnew +='{"name": "#'+tables[table]+'", \n"children": [ \n';
// 		var x = document.getElementById(tables[table])
// 		for (var row =0; row < x.rows.length; row+=1){
// 			outnew += '{ "row": "'+row+'",\n "cells": [\n'
// 			for (var cell=0; cell < x.rows[row].cells.length; cell +=1){
// 				outnew += '{"id": "'+(x.rows[row].cells[cell].innerText)+'"}'
// 				if (cell < x.rows[row].cells.length-1){
// 					outnew += ",\n"
// 				}
// 				else{
// 					outnew += "\n"
// 				}
// 			}
// 			outnew +="]\n}"
// 			if (row < x.rows.length-1){
// 				outnew +=",\n"
// 			}
// 			else{
// 				outnew += "\n"
// 			}
// 		}
// 		if (table < tables.length-1){
// 			outnew +="]\n},\n"
// 		}
// 		else{
// 			outnew += "]\n}"
// 		}
// 	}
// 	outnew +="]\n}\n"
// 	json_outnew = JSON.parse(outnew)
// 	return json_outnew
// }