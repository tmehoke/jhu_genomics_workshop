#!/bin/bash
Software_location=$HOME/Documents/Software/
Working_location=$( pwd )
KRAKEN_DIR="~/bin/kraken_dir"
BEAST_dir="~/bin/BEAST_dir"
Database_locaton=$HOME/Documents/database/
base='false'
post='false'

while getopts ":pb" opt; do
  case ${opt} in
    p ) 
	echo "removing post-hoc dependencies"
	post='true'
      ;;
    b ) 
	base='true'
	echo "removing all base dependencies"

      ;;
    \? ) echo "Usage: cmd 
			[-b]	Will uninstall all base dependencies 
			[-p]	Will uninstall all post-hoc installs"
      ;;
  esac
done


if [[ $base == 'true' ]]; then
	echo "removing base dependencies"
	sudo apt -y purge checkinstall
	sudo apt -y purge pigz
	sudo apt -y purge hdf5-tools
	sudo apt -y purge vim
	sudo apt -y purge libz-dev
	pip uninstall -y keras
	pip uninstall -y edlib
	pip uninstall -y noise
	pip uninstall -y h5py
	pip uninstall -y ont-tombo
	pip uninstall -y pandas
	sudo apt-get -y purge git
	sudo apt-get -y purge openjdk-8-jre
	sudo apt-get -y purge openjdk-8-jdk
	sudo dpkg --remove mafft
	sudo dpkg --remove megax
	sudo dpkg --remove pandoc
	sudo rm ~/bin/bin/jellyfish
	sudo rm -rf $Software_location/Porechop
	sudo rm -rf ~/bin/porechop-runner.py
	sudo rm -rf $KRAKEN_DIR
	sudo apt-get -y purge expect
	sudo apt-get -y purge gawk
	sudo apt-get -y purge gnome-terminal
	#Remove all make directories in bin from .bashrc
	if grep "export PATH=\"${KRAKEN_DIR}:\$PATH\"" ~/.bashrc; then 
		echo 'yes'; 
	fi
	if  grep  "export PATH=\"${KRAKEN_DIR}:\$PATH\"" ~/.bashrc; then
		echo "yes"
		# sed -e 's/export PATH="${KRAKEN_DIR}:$PATH"//g' ~/.bashrc
	fi
	if  grep -L  "export PATH=\"${BEAST_dir}:\$PATH\"" ~/.bashrc; then
		echo "export PATH=\"${BEAST_dir}:\$PATH\"" >> ~/.bashrc
	fi
fi
if [[ $post == 'true'  ]]; then
	echo "removing post-hoc dependencies"
	sudo apt-get -y purge ont-bream4-minion
	sudo apt-get -y purge minknow-nc
	sudo apt-get -y purge minknow-core-minion-nc
	sudo apt-get -y purge minion-nc
	sudo apt-get -y purge ont-minknow-frontend
	sudo apt-get -y purge ont-guppy
	sudo rm -rf /opt/ONT/MinKNOW
	#remove minknow makes an authentication error/corruption of gdm3, so purge and reinstall
	sudo apt-get install gdm3 ubuntu-desktop
fi