script_location="$(perl -MCwd=abs_path -le 'print abs_path(shift)' $(which $(basename $0)))"
# Software_location="$HOME/Documents/Software/"
KRAKEN_DIR="$HOME/bin/kraken_dir"
BEAST_dir="$HOME/bin/BEAST_dir"
LAUNCHER_dir="$HOME/.local/share/applications"
kraken_database_location="$HOME/Documents/database/flukraken-2019-06-24"
read -r -p "is the install designed for WSL (Windows Subystem for Linux? If \"y\", 
the system will finish installing after setting up
the proper WSL environment. 
Select \"n\" if you arent using WSL OR you've already selected \"y\" to 
set up the WSL environment [y/n]" response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]];
then
	sudo apt-get -y update
	sudo apt-get -y upgrade
	sudo apt-get -y install nodejs
	sudo apt-get -y install nodejs-legacy
	export DISPLAY=:0.0 #need to enable this every time so add to .bashrc
	if ! grep -L  "echo export DISPLAY=:0.0" ~/.bashrc; then
		echo export DISPLAY=:0.0 >> ~/.bashrc
		source ~/.bashrc
	fi
	echo "Done Installing, dont forget to check WSL document in base directory for further instructions"
	exit 1
fi


read -r -p "This script will install many base installation dependencies.
Sudo priv. is required to install these scripts.
 It will also create several directories in bin to add to your \$PATH.
 It will also delete your most recent version of numpy for python2 and reinstall 1.12.1 once running post-hoc_install.sh
 Are you sure you want to continue installing? [y/N] " response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]];
then
	if [[ ! -d "$kraken_database_location" ]]; then
		mkdir -p "$kraken_database_location"
	fi
	#Define the bin directory
	if [[ ! -d "$HOME/bin" ]]; then
		echo "Bin directory doesn't exist, making one in ${HOME}"
		mkdir -p "$HOME/bin"
	fi
	mkdir -p "$HOME/bin/bin"
	mkdir -p "$KRAKEN_DIR"
	mkdir -p "$BEAST_dir"

	if ! grep -L  "export PATH=\"\$HOME/bin:\$PATH\"" ~/.bashrc; then
		echo "export PATH=\"\$HOME/bin:\$PATH\"" >> ~/.bashrc
	fi
	if ! grep -L  "export PATH=\"\$HOME/bin/bin:\$PATH\"" ~/.bashrc; then
		echo "export PATH=\"\$HOME/bin/bin:\$PATH\"" >> ~/.bashrc
	fi
	if ! grep -L  "export PATH=\"${KRAKEN_DIR}:\$PATH\"" ~/.bashrc; then
		echo "export PATH=\"${KRAKEN_DIR}:\$PATH\"" >> ~/.bashrc
	fi
	if ! grep -L  "export PATH=\"${BEAST_dir}:\$PATH\"" ~/.bashrc; then
		echo "export PATH=\"${BEAST_dir}:\$PATH\"" >> ~/.bashrc
	fi
	

	

	# sudo dpkg -i "$script_location/zlib1g-dev_1.2.11.dfsg-0ubuntu2_amd64.deb"
	# sudo dpkg -i "$script_location/libbz2-dev_1.0.6-8.1ubuntu0.2_amd64.deb"
	# sudo dpkg -i "$script_location/liblzma-dev_5.2.2-1.3_amd64.deb"
	# sudo dpkg -i "$script_location/libncurses5-dev_6.1-1ubuntu1.18.04_amd64.deb"
	# sudo dpkg -i "$script_location/libtinfo-dev_6.1-1ubuntu1.18.04_amd64.deb"
	# sudo dpkg -i "$script_location/libncursesw5-dev_6.1-1ubuntu1.18.04_amd64.deb"
	sudo apt-get -y install libbz2-dev liblzma-dev libncurses5-dev libtinfo-dev zlib1g-dev
	####Jellyfish install for kraken build script
	jellyfish_version=$(jellyfish --version 2> /dev/null | head -n1)
	if [[ ! -x "$(command -v jellyfish)" ]] || ([ -L "jellyfish" ]) && ([ ! -a "jellyfish" ]); then
		wget http://www.cbcb.umd.edu/software/jellyfish/jellyfish-1.1.11.tar.gz -P "$script_location/Software/"
		tar -xvzf "$script_location/Software/jellyfish-1.1.11.tar.gz" --directory "$script_location/Software/"
		cd "$script_location/Software/jellyfish-1.1.11"
		./configure --prefix="$HOME/bin"
		make
		sudo make install
		cd "$script_location"
	fi
	if [[ ! -x "$(command -v java)" ]] || ([ -L "java" ]) && ([ ! -a "java" ]); then
		#java
		sudo apt-get -y install openjdk-8-jre
		sudo apt-get -y install openjdk-8-jdk
	fi
	samtools_version=$(samtools --version 2> /dev/null | head -n1)
	if [[ ! -x "$(command -v samtools)" ]] || ([ -L "samtools" ]) && ([ ! -a "samtools" ]); then
		wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 -P "$script_location/Software/"
		#Pre reqs to install for samtools
		sudo apt-get install gcc
		sudo apt-get -y install make
		sudo apt-get -y install libbz2-dev
		sudo apt-get -y install zlib1g-dev
		sudo apt-get -y install libncurses5-dev
		sudo apt-get -y install libncursesw5-dev
		sudo apt-get -y install liblzma-dev

		cd "$script_location/Software/"
		tar -xvjf samtools-*.tar.bz2
		cd "$script_location/Software/samtools-1.9"
		./configure --prefix="$HOME/bin/"
		make
		sudo make install
		cd "$script_location"
	fi 
	if [[ ! -x "$(command -v bcftools)" ]] || ([ -L "bcftools" ]) && ([ ! -a "bcftools" ]); then
		wget https://github.com/samtools/bcftools/releases/download/1.9/bcftools-1.9.tar.bz2 -P "$script_location/Software/"
		cd "$script_location/Software/"
		tar -xvjf bcftools*.tar.bz2
		cd "$script_location/Software/bcftools-1.9"
		./configure --prefix="$HOME/bin/"
		make
		sudo make install
		cd "$script_location"
	fi
	if [[ ! -x "$(command -v tracer)" ]] || ([ -L "tracer" ]) && ([ ! -a "tracer" ]); then
		#Tracer install
		wget https://github.com/beast-dev/tracer/releases/download/v1.7.1/Tracer_v1.7.1.tgz -P "$script_location/Software/"
		tar -xvzf "$script_location/Software/Tracer_v1.7.1.tgz" --directory "$script_location/Software/"
		sudo chmod 755 "$script_location/Software/Tracer_v1.7.1/bin/tracer"
		ln -sf "$script_location/Software/Tracer_v1.7.1/bin/tracer" ~/bin/
		ln -sf "$script_location/Software/Tracer_v1.7.1/lib/tracer.jar" ~/bin/
	fi

	if [[ ! -x "$(command -v figtree)" ]] || ([ -L "figtree" ]) && ([ ! -a "figtree" ]); then
		#figtree symlink checking doesnt work So just reinstall every time
		wget https://github.com/rambaut/figtree/releases/download/v1.4.4/FigTree_v1.4.4.tgz -P "$script_location/Software/"
		tar -xzvf "$script_location/Software/FigTree_v1.4.4.tgz" --directory "$script_location/Software/"
		echo "java -jar \"$script_location/Software/FigTree_v1.4.4/lib/figtree.jar\""> ~/bin/figtree.jar
		ln -sf ~/bin/figtree.jar ~/bin/figtree
		sudo chmod 755 ~/bin/figtree
	fi
	
	if [[ ! -x "$(command -v mafft)" ]] || ([ -L "mafft" ]) && ([ ! -a "mafft" ]); then
		#mafft
		wget https://mafft.cbrc.jp/alignment/software/mafft_7.427-1_amd64.deb -P "$script_location/Software/"
		sudo dpkg -i "$script_location/Software/mafft_7.427-1_amd64.deb"
	fi
	if [[ ! -x "$(command -v aliview)" ]] || ([ -L "aliview" ]) && ([ ! -a "aliview" ]); then
		#aliview
		wget http://ormbunkar.se/aliview/downloads/linux/linux-version-1.26/aliview.install.run -P "$script_location/Software/"
		cp -f "$script_location/Software/aliview.install.run" "$script_location/Software/"
		chmod +x "$script_location/Software/aliview.install.run"
		sudo bash "$script_location/Software/aliview.install.run" #run program with cmd: aliview
	fi
	if ! [ -x "$(command -v git)" ]; then
		##Install Additional packages
		sudo apt-get -y install git
	fi
	
	if [[ ! -x "$(command -v porechop)" ]] || ([ -L "porechop" ]) && ([ ! -a "porechop" ]); then
		#Pore chop
		sudo apt-get -y install python3-distutils
		pip3 install setuptools
		cd "$script_location/Software/"
		git clone https://github.com/rrwick/Porechop.git
		cd "Porechop"
		sudo python3 setup.py install
		cd "$script_location"
	fi
	#Build script for kraken dependencies and pathing
	if [[ ! -x "$(command -v kraken)" ]] || ([ -L "kraken" ]) && ([ ! -a "kraken" ]); then
		cd "$script_location/Software"
		git clone https://github.com/DerrickWood/kraken.git 
		cd "kraken"
		bash install_kraken.sh "$KRAKEN_DIR"
		cp "$script_location/Software/kraken/scripts/"* "$KRAKEN_DIR"
		cp "$script_location/Software/kraken/src/"* "$KRAKEN_DIR"
		cd "$script_location"
	fi
	if [[ ! -x "$(command -v beast)" ]] || ([ -L "beast" ]) && ([ ! -a "beast" ]); then
		#Install BEAST
		wget https://github.com/beast-dev/beast-mcmc/releases/download/v1.10.4/BEASTv1.10.4.tgz -P "$script_location/Software/"
		tar -zxvf "$script_location/Software/BEASTv1.10.4.tgz" --directory "$script_location/Software/"
		find "$script_location/Software/BEASTv1.10.4/bin/" -type f -print0 | while read -d $'\0' fn; do ln -sf "$fn" "$BEAST_dir"; done
		if ! grep -L  "export PATH=\"$script_location/Software/BEASTv1.10.4/bin:\$PATH\"" ~/.bashrc; then
			echo "export PATH=\"$script_location/Software/BEASTv1.10.4/bin:\$PATH\"" >> ~/.bashrc
		fi
		cd "$script_location"
	fi
	#get blast executables and compile from source (compield origin on redhat so get some c++ errors)
	if [[ ! -x "$(command -v blastn)" ]] || ([ -L "blastn" ]) && ([ ! -a "blastn" ]); then
		wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.8.1/ncbi-blast-2.8.1+-x64-linux.tar.gz -P "$script_location/Software/"
		tar -xvzf "$script_location/Software/ncbi-blast-2.8.1+-x64-linux.tar.gz" --directory "$script_location/Software/"
		find "$script_location/Software/ncbi-blast-2.8.1+/bin/" -type f -print0 | while read -d $'\0' fn; do sudo cp "$fn" ~/bin/bin/; done
	fi
	if [[ ! -x "$(command -v seqtk)" ]] || ([ -L "seqtk" ]) && ([ ! -a "seqtk" ]); then
		cd "$script_location/Software"
		git clone https://github.com/lh3/seqtk.git
		cd seqtk
		make
		ln -sf "$script_location/Software/seqtk/seqtk" "$HOME/bin"
		cd "$script_location"
	fi
	ln -sf "$script_location/../app/scripts/sam2bam.sh" "$HOME/bin"
	ln -sf "$script_location/../app/scripts/vcfutils.pl" "$HOME/bin"
	#go ahead and cd to the app  after installing electron globally and npm install everything
	sudo apt-get install nodejs
	sudo apt-get -y install npm
	cd "$script_location/../app"
	npm install
	npm i electron
	npm install dialog
	npm install jquery
	npm install mkdirp
	#For opening terminal with populated fields (replacement to gnome-terminal)
	sudo apt-get -y install lxterminal

	rm -rf "$script_location/../app/tabs/mytax"
	git clone https://github.com/tmehoke/mytax.git "$script_location/../app/tabs/mytax"
	find "$script_location/../app/scripts" -name "*.sh" -print0 | while read -d $'\0' fn; do sudo ln -sf "$fn" ~/bin/; done
	#Symlink tmehoke flukraken db scripts to $PATH
	find "$script_location/../app/tabs/mytax" -name "*.sh" -print0 | while read -d $'\0' fn; do sudo ln -sf "$fn" "$KRAKEN_DIR"; done
	
	sudo python2 -m pip uninstall --yes numpy


#	sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
	if ! [ -x "$(command -v pip)" ]; then
		sudo apt-get -y install python-pip
		sudo apt-get -y install python3-pip
	fi
	sudo apt -y install checkinstall
	sudo apt -y install pigz
	sudo apt -y install hdf5-tools
	pip install h5py

	sudo apt-get -y install libgconf-2-4
	sudo apt-get -y install libz-dev #need for make command of minimap2
	sudo apt-get -y install libbz2-dev #need for make command of minimap2
	sudo apt-get -y install libncurses5-dev libncursesw5-dev #need for make command of minimap2


	#Install expect in order to click buttons to preload commands to terminal
	sudo apt-get -y install gnome-terminal
	sudo apt-get -y install expect
	sudo apt-get -y install gawk

	source ~/.bashrc #finally, source bashrc to get PATH changes


	#add split_segments to global variable
	ln -sf "$script_location/../app/scripts/split_segments.sh" "$HOME/bin/bin"



	# create launcher and add to favorites
#	sed "s@<SCRIPT_LOCATION>@$script_location/../@g" "$script_location/Workshop.desktop" > "$LAUNCHER_dir/jhu_genomics_workshop.desktop"
#	favorites=$(dconf read /org/gnome/shell/favorite-apps)
#	echo "$favorites" | sed "s/\[/\[\'jhu_genomics_workshop.desktop\', /"
#	dconf write /org/gnome/shell/favorite-apps "$(echo "$favorites" | sed "s/\[/\[\'jhu_genomics_workshop.desktop\', /")"

	# add correct path variables to renderer.js
	sed -e "s@<KRAKEN_DIR>@$kraken_database_location@g" \
		-e "s@<APP_DIR>@$script_location/../app@g" \
		"$script_location/renderer.js" > "$script_location/../app/renderer.js"

	# add correct path variables to main.js
	sed -e "s@<APP_DIR>@$script_location/../app@g" \
		"$script_location/main.js" > "$script_location/../app/main.js"
fi
