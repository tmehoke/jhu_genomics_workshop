#!/bin/bash

#---------------------------------------------------------------------------------------------------
# deal with local environment

# pull nanosecond runtime for temporary file directory
runtime=$(date +"%Y%m%d-%H%M%S-%N")

# define colors for error messages
red='\033[0;31m'
RED='\033[1;31m'
green='\033[0;32m'
GREEN='\033[1;32m'
yellow='\033[0;33m'
YELLOW='\033[1;33m'
blue='\033[0;34m'
BLUE='\033[1;34m'
purple='\033[0;35m'
PURPLE='\033[1;35m'
cyan='\033[0;36m'
CYAN='\033[1;36m'
NC='\033[0m'

# usage function
usage() {
	echo -e "usage: ${YELLOW}$0${NC} [options]"
	echo -e ""
	echo -e "OPTIONS:"
	echo -e "   -h      show this message"
	echo -e "   -k      Kraken database folder (path)"
	echo -e "             Note: database must contain:"
	echo -e "                     seqid2taxid.map map file"
	echo -e "                     library FASTA files"
	echo -e "   -i      kraken report with fullstring"
	echo -e "   -o      output file"
	echo -e "   -l      logfile"
	echo -e "   -w      temporary directory (default: ${CYAN}/tmp${NC})"
	echo -e ""
}

#---------------------------------------------------------------------------------------------------
# set default values here
tempdir="/tmp"
logfile=/dev/null

# parse input arguments
while getopts "hk:i:o:l:w:" OPTION
do
	case $OPTION in
		h) usage; exit 1 ;;
		k) krakenDB=$OPTARG ;;
		i) fullstring=$OPTARG ;;
		o) outputFile=$OPTARG ;;
		l) logfile=$OPTARG ;;
		w) tempdir=$OPTARG ;;
		?) usage; exit 1 ;;
	esac
done

# make sure kraken database exists
if ! [[ -d "$krakenDB" ]]; then
	echo -e "\n${RED}Error: specified kraken database ${YELLOW}\"$krakenDB\"${NC} does not exist.${NC}\n" >&2
	usage
	exit 2
else
	map_file=$(find -L "$krakenDB" -name "seqid2taxid.map")
	if [[ -z "$map_file" ]]; then
		echo -e "\n${RED}Error: specified kraken database library ${YELLOW}\"$krakenDB\"${RED} does not have the required map file ${YELLOW}\"seqid2taxid.map\".${NC}\n" >&2
		exit 2
	elif ! [[ -f "$map_file" ]]; then
		echo -e "\n${RED}Error: specified kraken database library ${YELLOW}\"$krakenDB\"${RED} does not have the required map file ${YELLOW}\"seqid2taxid.map\".${NC}\n" >&2
		exit 2
	fi
fi

# make sure kraken report file exists
if ! [[ -f "$fullstring" ]]; then
	echo -e "\n${RED}Error: specified fullstring kraken report ${YELLOW}\"$fullstring\"${NC} does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# check to see if output file was specified
if [[ -z "$outputFile" ]]; then
	echo -e "\n${RED}Error: please specify an output file with the -o flag.${NC}\n" >&2
	usage
	exit 2
fi

# check to see if output file exists
if [[ -f "$outputFile" ]]; then
	echo -e "\n${YELLOW}Warning: specified output file already exists.${NC}\n" >&2
fi

# define default logfile
if [[ -z "$logfile" ]]; then
	logfile="$outputPath/get_closest_reference.log"
fi

#---------------------------------------------------------------------------------------------------
# error checking


#---------------------------------------------------------------------------------------------------
# dependency checking (not implemented yet)


#===================================================================================================
# defining functions
#===================================================================================================

echo_log() {

	input="$*"

	# if input is non-empty string, prepend initial space
	if [[ -n "$input" ]]; then
		input=" $input"
	fi

	# print to STDOUT
	echo -e "[$(date +"%F %T")]$input"

	# print to log file (after removing color strings)
	echo -e "[$(date +"%F %T")]$prefix$input" | gawk '{ printf("%s\n", gensub(/\x1b\[[0-9;]*m?/, "", "g", $0)); }' >> "$logfile"
}

#---------------------------------------------------------------------------------------------------
# remove line breaks from the sequences in a FASTA file
fasta_rmlinebreaks_2col() {

	input="$*"

	gawk '{
		if(NR == 1) {
			printf("%s\t", $0);
		} else {
			if(substr($0,1,1) == ">") {
				printf("\n%s\t", $0);
			} else {
				printf("%s", $0);
			}
		}
	} END {
		printf("\n");
	}' "$input"

}

#---------------------------------------------------------------------------------------------------

#===================================================================================================
# Main body
#===================================================================================================

# create environment
workdir="$tempdir/get_closest_reference-$runtime"
mkdir -p "$workdir"

#===================================================================================================

# report current hash of git repo
#GIT_DIR="$(dirname $(readlink -f $(which $(basename $0))))/.git"
#export GIT_DIR
#hash=$(git rev-parse --short HEAD)
hash="NA"

# set up log file
echo_log "------ Call to "${YELLOW}$(basename $0)${NC}" from "${GREEN}$(hostname)${NC}" ------"
echo_log "current git hash: $hash"
echo_log "input arguments:"
echo_log "  kraken database: ${CYAN}$krakenDB${NC}"
echo_log "    map file: ${CYAN}$map_file${NC}"
echo_log "  kraken report: ${CYAN}$fullstring${NC}"
echo_log "output files:"
echo_log "  output file: ${CYAN}$outputFile${NC}"
echo_log "  log file: ${CYAN}$logfile${NC}"
echo_log "------ beginning analysis ------"

# grab highest hit for each segment in kraken report
gawk -F $'\t' '{
	if(NR==FNR) {
		header[$2] = $1;
	} else {
		if($7 ~ /\(strain\)$/) {
			flu_type = gensub(/.*Influenza ([A-D]) virus\(species\)\|.*/, "\\1", "g", $7);
			segment = gensub(/.*;([1-8]) \((.*)\)\(segment\)\|.*/, "\\1_\\2", "g", $7);
			type_segment = sprintf("%s-%s", flu_type, segment);
			if(!(type_segment in consensus)) {
				consensus[type_segment] = 1;
				strain_taxid = gensub(/.*\|([0-9]+);[^\|]+\(strain\).*/, "\\1", "g", $7);
				printf("%s\t%s\t%s\t%s\n", flu_type, segment, strain_taxid, header[strain_taxid]);
			}
		}
	}
}' "$map_file" "$fullstring" > "$workdir/strain_headers"

# get sequence for FASTA header
gawk -F $'\t' -v OUTPUTBASE="$workdir/output-" '{
	if(NR==FNR) {
		type[$4] = $1;
	} else {
		split(substr($1, 2), h, " ");
		if(h[1] in type) {
			filename=sprintf("%s%s", OUTPUTBASE, type[h[1]]);
			printf(">%s\n%s\n", h[1], $2) >> filename;
		}
	}
}' "$workdir/strain_headers" <(fasta_rmlinebreaks_2col "$krakenDB/library/IVR/influenza.fna")

# print to output files
if [[ -n "$outputFile" ]]; then
	find "$workdir" -name "output-*" | while read fn; do
		base=$(basename "$fn")
		type="${base#output-}"
		ext="${outputFile##*.}"
		sed 's/:/_/g' "$fn" > "${outputFile%.*}-I${type}V.$ext"
	done
fi

rm -rf "$workdir"

echo_log "${GREEN}Done "$(basename $0)".${NC}"
