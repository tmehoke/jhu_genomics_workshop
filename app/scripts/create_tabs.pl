#!/usr/bin/perl
use strict; use warnings; 
use HTML::DOM
open (my $fh, "<", $ARGV[0]);
my %tabs = ();
my $end = "<p>-------------</p>\n";
while (my $row = <$fh>){
	chomp $row;
	if ($row =~ /\<h[0-9]{1,}/g){
		my ( $id ) = $row =~ /\sid="(.+?)"/g;
		my ( $value ) = $row =~ /\>(.+?)\</g;
		$end .= "<button class=\"tablinks\" id=$id>$value</button>\n"
	}
}
print $end."\n";
close $fh;
my $other_dom_tree = new HTML::DOM;

$other_dom_tree->parse_file($ARGV[1]);
 
$other_dom_tree->getElementsByTagName('body')->[0]->appendChild(
         $other_dom_tree->createElement('input')
);
 
print $other_dom_tree->innerHTML, "\n";