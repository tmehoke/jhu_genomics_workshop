import pandas as pd; import numpy as np; 
import argparse
import re
import time;
from scipy import stats
import math
from bs4 import BeautifulSoup
parser = argparse.ArgumentParser(description = "Gather proportion of GO Terms that are located in enriched list")
parser.add_argument('-i', required = True, type=str, nargs='+', help = 'Input file (html) to add the tab (scrolling)')
parser.add_argument('-o', required = True, type=str, nargs='+', help = 'output file containing newly edited html file')
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
args = parser.parse_args()
with open(vars(args)['i'][0], 'r') as file:
    data = file.read().replace('\n', '')
##########Read in the input file
soup = BeautifulSoup(data, 'html.parser')

h =soup.body
main_body = soup.new_tag("div", **{'class':'main'}, id="main_body")
print(main_body)
tab = soup.new_tag("div", **{'class':'tab'})
main_body.append(tab)
pf = main_body.find("div", {"class": "tab"})
addition = []
print(main_body)
for e in (h.find_all(['h1','h2', 'h3', 'h4', 'h5', 'strong','span', 'p'])):
	if e.get("id"):
		but = soup.new_tag("button", \
		id=e.get("id")+"-place", \
		onclick="scroll2(\""+e.get("id")+"-place\")"
		)
		but['class'] = "tablinks"
		but.string = e.string
		pf.append(but)
body_children = list(h.children)
content = soup.new_tag("div", **{'class':'content'})
main_body.append(content)

for e in body_children:
	content.append(e)
for e in soup.find("body").findChildren(recursive=False):
	e.decompose()
soup.find("body").append(main_body)
print(soup)
button_function = """

function scroll2(idmain){
	var elmnt = document.getElementById(idmain);
	var topPos = elmnt.offsetTop;
	pf = elmnt.id
	id = pf.substring(0, pf.length - 6);
	var elmnt = document.getElementById(id); 
	elmnt.scrollIntoView(); 
}
"""

script = soup.new_tag("script")
script.string=button_function
soup.append(script)
	
with open(vars(args)['o'][0], "w") as file:
    file.write(str(soup.prettify()))