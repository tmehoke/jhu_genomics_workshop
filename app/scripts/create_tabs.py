import pandas as pd; import numpy as np; 
import argparse
import re
import time;
from scipy import stats
import math
from bs4 import BeautifulSoup
parser = argparse.ArgumentParser(description = "Gather proportion of GO Terms that are located in enriched list")
parser.add_argument('-i', required = True, type=str, nargs='+', help = 'Input file (html) to add the tab (scrolling)')
parser.add_argument('-o', required = True, type=str, nargs='+', help = 'output file containing newly edited html file')
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
args = parser.parse_args()
with open(vars(args)['i'][0], 'r') as file:
    data = file.read().replace('\n', '')
##########Read in the input file
soup = BeautifulSoup(data, 'html.parser')

for elem in soup(id=re.compile(r'^section-?[0-9]{0,}$')):
    elem.decompose()



h = soup.find_all("a")
addition = []
for e in (soup.find_all(['h1','h2'])):
	if (len(e.text) ==0):
		e.string = e.get("id");
	plo=(e.find("strong"))
	if plo is not None:
		e.string = plo.text
	clas = ""
	if (e.name =='h1'):
		clas = "tablinks"
	else:
		clas = "tablinks_sub"
	if e is not None:	
		if e.get("id") is not None:
			addition.append([e.get("id"), e.text, clas])

# print(soup.body.findChildren(recursive=False))
hk = ''.join(['%s' % x for x in soup.body.findChildren(recursive=False)])





####Read in the output file for html
with open(vars(args)['o'][0], 'r') as file:
    data = file.read().replace('\n', '')
soup = BeautifulSoup(data, 'html.parser')

i = soup.find("div", {"id": "content"})

i.append(BeautifulSoup(hk, "html.parser"))
# print(i.prettify())



h =(soup.find("div", {"id": "tab"}))
p=soup.new_tag("p", id="delimit")
p.string="_________"*200
h.append(p)
p=soup.new_tag("h1", **{'class':'tab_top'})
p.string="Sections"
h.append(p)
for e in addition:
	but = soup.new_tag("button", \
		id=e[0]+"-place", \
		onclick="scroll2(\""+e[0]+"-place\")"
		)
	but['class'] = e[2]
	but.string =e[1]
	h.append(but)

	
with open("new.html", "w") as file:
    file.write(str(soup.prettify()))