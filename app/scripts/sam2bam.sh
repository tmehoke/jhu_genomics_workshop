#!/bin/bash

# automatically exit the script on any error
set -e

sam="$@"
base="${sam%.sam}"

# for samtools version
#samtools view -bSh "$base.sam" -o "$base.unsorted.bam" -@ $THREADS
#samtools sort -O "bam" -T "$base-temp" -o "$base.bam" -@ $THREADS "$base.unsorted.bam" && rm "$base.unsorted.bam"
#samtools index "$base.bam"
#samtools reheader "$base.sam" "$base.bam" > "$base-temp" && mv "$base-temp" "$base.bam" && rm "$sam"

# for samtools version 1.2-66-g44e1a74-dirty
samtools view -bSh "$sam" -o "$base.unsorted.bam"
samtools sort -O "bam" -T "$base-temp" -o "$base.bam" "$base.unsorted.bam" && rm "$base.unsorted.bam"
#samtools reheader "$sam" "$base.unsorted.bam" > "$base-temp" && mv "$base-temp" "$base.unsorted.bam"
samtools reheader "$sam" "$base.bam" > "$base-temp" && mv "$base-temp" "$base.bam" # && rm "$sam"
samtools index "$base.bam"
