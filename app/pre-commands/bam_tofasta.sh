#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR1 [lindex $argv 1]
set MYVAR2 [lindex $argv 2]
set MYVAR3 [lindex $argv 3]


# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "sam2bam.sh $MYVAR && bam2fasta.sh \\\n\t -r $MYVAR1 \\\n\t -b $MYVAR2 -o $MYVAR3"
# Hand over control to the user
interact

exit
