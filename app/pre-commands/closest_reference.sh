#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR1 [lindex $argv 1]
set MYVAR2 [lindex $argv 2]


# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "get_closest_reference.sh \\\n\t -i $MYVAR \\\n\t -k $MYVAR1 \\\n\t -o $MYVAR2"
# Hand over control to the user
interact

exit
