#####*IMPORTANT*
This repo is under construction and will have a new home posted here soon.

README

Go into Install_Scripts and run 

####*OPTIONAL*

`bash Base_install.sh` to ensure that all standard dependencies met


####*Required*

To Install MinKNOW and Guppy...

`bash post-hoc_install.sh`

If there are any unmet dependencies, please install them (will be output through stdout)

and go to https://www.megasoftware.net/

Download the Ubuntu Distribution and run 

`sudo dpkg -i path/to/the/deb/package`

to Install

Then go to 

`app` and run 
all of these (install individual) should be taken care of in node_modules but just to be safe

`npm install` 

`npm install dialog`

`npm install child_process`

`npm install jquery`

to retrieve all node.js necessary modules. And finally, run 

`electron .` to begin the app!


`uninstall.sh` has 2 arguments and is used to remove all base_dependencies [-b] and all post-hoc install dependencies [-p]

It is recommended to go through the base_uninstallation script if you've used these dependencies elsewhere before uninstalling